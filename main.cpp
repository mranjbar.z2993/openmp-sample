#include <iostream>
#include <sstream>
#include <string>
//#include <cstdlib>
//#include <ctime>
#include <random>

int generateRandomNumber(int min, int max);

int main() {
    std::cout << "Hello, world!\n";
    std::cout << "before filling \n" ;

    int rows = 10000;
    int columns = 10000;
    int min = 1;
    int max = 100;

    int **arr = new int *[rows];
    for (int i = 0; i < rows; i++)
        arr[i] = new int[columns];

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            arr[i][j] = generateRandomNumber(0,3);
        }
    }
    std::cout << "matrix filled \n" ;

    const clock_t begin_time = clock();
// do something
    int sum = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            sum += arr[i][j] ;
        }
    }
    std::cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC;
    std::cout << "matrix sum " + std::to_string(sum) + "\n";

}

int generateRandomNumber(int min, int max) {
    int randomNumber = std::rand() ;
    int formattedRandomNumber = (randomNumber % (max - min)) ;
    return formattedRandomNumber;
}