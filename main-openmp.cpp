//Mohammad Ranjbar Zaferanie, 97422117

//g++  5.4.0
#include <iostream>
#include <stdio.h>
#include <omp.h>
#include <sstream>
#include <string>

void setThreadNum();
int generateRandomNumber(int min, int max);


void setThreadNum(){
    int threadNum = omp_get_num_procs();
    omp_set_num_threads(threadNum);
}

int main() {
    setThreadNum();
    std::cout << "filling matrix ...\n" ;

    int rows = 10000;
    int columns = 10000;
    int **arr = new int *[rows];
    for (int i = 0; i < rows; i++)
        arr[i] = new int[columns];

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            arr[i][j] = generateRandomNumber(0,3);

        }
    }
    std::cout << "matrix filled \n" ;
    std::cout << "matrix rows " + std::to_string(rows)+"    matrix columns " + std::to_string(columns)  + "\n";

    const clock_t begin_time = clock();
// do something
    int sum = 0;
#pragma omp parallel for reduction(+:sum)
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            sum += arr[i][j] ;
        }
    }
    std::cout << "matrix sum " + std::to_string(sum) + "\n";
    float withOmp = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
    std::cout << "matrix sum time with omp " + std::to_string(withOmp) +"\n\n";



    int sum2 = 0;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            sum2 += arr[i][j] ;
        }
    }

    std::cout << "matrix sum " + std::to_string(sum2) + "\n";
    float withoutOmp = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
    std::cout << "matrix sum time without omp " + std::to_string(withoutOmp)+"\n";


    std::cout << "Speed up is " + std::to_string(withoutOmp/withOmp)+"\n";



}

int generateRandomNumber(int min, int max) {
    int randomNumber = std::rand() ;
    int formattedRandomNumber = (randomNumber % (max - min)) ;
    return formattedRandomNumber;
}